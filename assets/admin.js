
$('.modal').modal();

// console.log('a')


$.ajax ({
	type: 'post',
	url: $('#base').val()+"admin/all_products",
	data:{admin: $('#admin').val()},
	dataType:'json',
	success:r=>{

		
		var table = $('<table class="tbl bordered"> </table>')

		if(r[0]){

			var tr =$('<tr class="thead-dark trt"> </tr>')
			var th = $('<th class="th"> </th>')
			var td = $('<td class="td"> </td>')
			var keys =  Object.keys(r[0])

			keys.forEach(function(item) {

				var th = $('<th> </th>')

				th.html(item) 
				th.appendTo(tr)
			})

			th.html('check')
			th.appendTo(tr)
			th.appendTo(tr)
			$('<th class="th"> Decline</th>').appendTo(tr)
			tr.appendTo(table)       
			table.appendTo('#tab')



			for (i=0; i<r.length; i++){
				var trt  = $('<tr class="thead-dark trt"> </tr>')

				$.each(r[i],function(index,value){

					if(index == "photo"){

						var tdd= $('<td class="td"></td>')
						tdd.html('<img src ="../uploads/'+value+'" style="width:120px; height:120px"> ')

						tdd.appendTo(trt)
						trt.appendTo(table)



					}else{



						var tdd= $('<td class="td"> </td>')
						tdd.html(value)

						tdd.appendTo(trt)
						trt.appendTo(table)
					}





				});
				tdd= $('<td class="td"> </td>')
				tdd1= $('<td class="td"> </td>')

				tdd.html('<button class="btn acept" data-id="'+r[i].id+'"> Acept </button>')
				tdd.appendTo(trt)
				tdd1.html('<a class="waves-effect waves-light btn modal-trigger" href="#modal1"><button class="btn decline modal-action modal-close waves-effect waves-green btn-flat" data-id="'+r[i].id+'" data-idd="'+r[i].user_id+'"> Decline </button></a>')
				tdd1.appendTo(trt)
					// $('<button class="btn" data-id="'+r[i].id+'"> decline </button>').appendTo(trt)
					trt.appendTo(table)

					table.appendTo('#tab')
				}
			}
		}
	})

$('body').on('click', ".acept" , function(){

	$(this).parents('.trt').hide(1000)
	$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/change_status",
		data:{id: $(this).data('id')},
	// dataType:'json',
	success:r=>{
		
	}

})

})


$('#btnAllUsers').on('click',function(){

	$('#allUsers').empty()
	$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/all_users",
		data:{},
		dataType:'json',
		success:r=>{
			


			var table = $('<table class="tbl bordered"> </table>')

			if(r[0]){

				var tr =$('<tr class="thead-dark trt"> </tr>')
				var th = $('<th class="th"> </th>')
				var td = $('<td class="td"> </td>')
				var keys =  Object.keys(r[0])

				keys.forEach(function(item) {

					var th = $('<th> </th>')

					th.html(item) 
					th.appendTo(tr)
				})
				th.html('Block')

				th.appendTo(tr)
				tr.appendTo(table)       
				table.appendTo('#tab')

			}

			for (i=0; i<r.length; i++){
				var trt  = $('<tr class="thead-dark trt"> </tr>')

				$.each(r[i],function(index,value){
					if(index == 'password'){

						var tdd= $('<td class="td"> </td>')
						tdd.html('*******')

						tdd.appendTo(trt)
						trt.appendTo(table)

					}else {




						var tdd= $('<td class="td"> </td>')
						tdd.html(value)

						tdd.appendTo(trt)
						trt.appendTo(table)


					}



				});
				tdd= $('<td class="td"> </td>')
				tdd.html('<button class="btnBlock" data-id="'+r[i].id+' "> Block </button>')
				tdd.appendTo(trt)
				trt.appendTo(table)
				table.appendTo('#allUsers')
			}




		}
	})

	

})


$('body').on('click', '.btnBlock', function(){

	var x = prompt('set time')

	$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/block_users",
		data:{time: x, id: $(this).data('id')},
		dataType:'json',
		success:r=>{ 


		}
	})
})

$('body').on('click', '.decline ', function(){
	

	$('#prodDecBtn').attr('data-id',$(this).data('id'))
	$('#prodDecBtn').attr('data-idd',$(this).data('idd'))
})




$('body').on('click','#prodDecBtn', function(){
	var id = $(this).data('id')
	$(`.decline[data-id=${id}]`).parents('tr').hide(1000)

	
	$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/delete_product",
		data:{id: $(this).data('id'), user_id: $(this).data('idd'), text: $('#textarea1').val()},
		dataType:'json',
		success:r=>{ 


		}

	})

})

$('#showOrders').on('click',function(){


	$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/all_orders",
		data:{},
		dataType:'json',
		success:r=>{


			var table = $('<table class="tbl bordered"> </table>')

			if(r[0]){

				var tr =$('<tr class="thead-dark trt"> </tr>')
				var th = $('<th class="th"> </th>')
				var td = $('<td class="td"> </td>')
				var keys =  Object.keys(r[0])

				keys.forEach(function(item) {
					
					var th = $('<th> </th>')

					th.html(item) 
					th.appendTo(tr)
					
				})
				th.html('Modal')

				th.appendTo(tr)
				tr.appendTo(table)       
				table.appendTo('#tab')

			}

			for (i=0; i<r.length; i++){
				var trt  = $('<tr class="thead-dark trt"> </tr>')

				$.each(r[i],function(index,value){
					
					var tdd= $('<td class="td"> </td>')
					tdd.html(value)

					tdd.appendTo(trt)
					trt.appendTo(table)
					
				});
				tdd= $('<td class="td"> </td>')
				tdd.html('<a class="waves-effect waves-light btn modal-trigger btnShowProduct" data-id="'+r[i].id+'" href="#modal2">Add<i class="material-icons right">add</i></a>')
				tdd.appendTo(trt)
				trt.appendTo(table)
				table.appendTo('#allOrders')
			}

		}
	}) 
})

$('body').on('click','.btnShowProduct', function(){

$.ajax ({
		type: 'post',
		url: $('#base').val()+"admin/show_orders_products",
		data:{id:$(this).data('id')},
		dataType:'json',
		success:r=>{
				var table = $('<table class="tbl bordered"> </table>')

		if(r[0]){

			var tr =$('<tr class="thead-dark trt"> </tr>')
			var th = $('<th class="th"> </th>')
			var td = $('<td class="td"> </td>')
			var keys =  Object.keys(r[0])

			keys.forEach(function(item) {

				var th = $('<th> </th>')

				th.html(item) 
				th.appendTo(tr)
			})

			// th.html('check')
			// th.appendTo(tr)
			// th.appendTo(tr)
			// $('<th class="th"> Decline</th>').appendTo(tr)
			tr.appendTo(table)       
			table.appendTo('#tab')
				for (i=0; i<r.length; i++){
				var trt  = $('<tr class="thead-dark trt"> </tr>')

				$.each(r[i],function(index,value){
					if(index == 'photo'){

					var tdd= $('<img style="width:100px;height: 100px" src="'+$('#base').val()+'/uploads/'+r[i].photo+'"> ')
						

						tdd.appendTo(trt)
						trt.appendTo(table)

					}else {




						var tdd= $('<td class="td"> </td>')
						tdd.html(value)

						tdd.appendTo(trt)
						trt.appendTo(table)


					}



				});
				// tdd= $('<td class="td"> </td>')
				// tdd.html('<button class="btnBlock" data-id="'+r[i].id+' "> Block </button>')
				// tdd.appendTo(trt)
				// trt.appendTo(table)
				table.appendTo('#order_table')
			}




			}
		}
			// 
		
	})


	
})