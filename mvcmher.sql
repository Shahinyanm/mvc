/*
Navicat MySQL Data Transfer

Source Server         : DT
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : mvcmher

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-01-29 12:38:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `invites`
-- ----------------------------
DROP TABLE IF EXISTS `invites`;
CREATE TABLE `invites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `asd` (`user_id`),
  CONSTRAINT `asd` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invites
-- ----------------------------
INSERT INTO `invites` VALUES ('4', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('5', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('6', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('7', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('8', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('9', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('10', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('11', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('12', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('13', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('14', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('15', 's-mher@inbox.ru', '1');
INSERT INTO `invites` VALUES ('16', 's-mher@inbox.ru', '1');

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `m_u` (`user_id`),
  CONSTRAINT `m_u` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('3', 'asdadad', '1');
INSERT INTO `messages` VALUES ('18', '121asad', '1');
INSERT INTO `messages` VALUES ('19', 'PPPPPP', '1');
INSERT INTO `messages` VALUES ('20', 'asdadsadsadsadsadasd', '1');
INSERT INTO `messages` VALUES ('21', '', '1');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ads` (`user_id`),
  CONSTRAINT `ads` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('2', '1', '2018-01-27 14:32:57');

-- ----------------------------
-- Table structure for `orders_products`
-- ----------------------------
DROP TABLE IF EXISTS `orders_products`;
CREATE TABLE `orders_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orders_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `a` (`products_id`),
  KEY `s` (`orders_id`),
  CONSTRAINT `a` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `s` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of orders_products
-- ----------------------------
INSERT INTO `orders_products` VALUES ('1', '2', '1');
INSERT INTO `orders_products` VALUES ('2', '2', '8');
INSERT INTO `orders_products` VALUES ('3', '2', '9');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `g_u` (`user_id`),
  CONSTRAINT `g_u` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', '', '125.00', 'Jellyfish.jpg', '2', '', '1');
INSERT INTO `products` VALUES ('8', 'asdadadad', '83.00', '0-02-04-7c6548c81dfb25d21a1f4ace73a2abf859764e8a9b17b5f9bf25e64382bd037e_full.jpg', '1', 'sadadadaad', '1');
INSERT INTO `products` VALUES ('9', 'Nor apranq ', '55.00', 'scrin1.png', '1', 'Nor apranq 1 sa ', '1');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Mher', 'Shahinyan', 'shahinyanm@gmail.com', '$2y$10$AwkUgTzaFAm4h.nkI0HKMO9015Lj9S8qE4MDIpPKbORIHD1WOdl/.', '27', '1');
INSERT INTO `users` VALUES ('2', 'Edo ', 'Hovhannisyan', 'edhovhannisyan@gmail.com', '$2y$10$251yZj9GFOeXKLliSLMMk.kAPH39ymbeMScBzNsAuRk9ufD2Vxnje', '21', '0');
