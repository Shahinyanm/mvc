<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
	<?php for($i = 1; $i <$tiv; $i++ ): ?>
    <a href="<?= base_url('products/show/'.$i) ?>"> <button id="show"><?= $i ?> </button></a>
  <?php endfor ?>


  <?php if (isset($result)): ?>
  	<?php foreach ($result as $value): ?>
  		<div class="row">
        <div class="col s3 m3">
          <div class="card">
            <div class="card-image">
              <img style="width:100px;height: 100px" src="<?= base_url("uploads/$value->photo") ?>">
              <span class="card-title"><?= $value->name ?></span>
            </div>
            <div class="card-content" style="min-height:100px;  overflow: auto">
              <p><?= $value->description ?>	</p>
              <p style="color:red">price: <?= $value->price ?> $	</p>
            </div>
            <div class="card-action">
            	
            	 <p>
    			 <input type="checkbox" id="test5<?= $value->id ?>" class="add" data-id="<?= $value->id ?>" >
   				 <label for="test5<?= $value->id ?>">Check</label>
   				</p>
   			
            </div>
          </div>
        </div>
  	<?php endforeach ?>
  <?php endif ?>


  <?php require_once('./config.php'); ?>

<form action="<?= base_url("products/pay")?>" method="post">
	<input type="hidden" name="prodId"  id="prodId">
  <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
          data-key="<?php echo $stripe['publishable_key']; ?>"
          data-description="Access for a year"
          data-amount="5000"
          data-locale="auto"></script>
</form>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

<script src="<?=base_url('assets/products.js');?>"></script>
</html>