<?php 
$admin = $this->session->userdata('admin'); 

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- Compiled and minified CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">



</head>
<body>
	

<div class="row">
	<div class="col s10" id="tab"> </div>
	<div class="col s2"> <button class="btn" id="btnAllUsers"> All Users</button> </div>
	 <!-- Modal Trigger -->
  <!-- <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Modal</a> -->

  <!-- Modal Structure -->
  <div id="modal1" class="modal bottom-sheet">
    <div class="modal-content">
     <div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea"></textarea>
          <label for="textarea1">Textarea</label>
        </div>
        <div class = 'col s4'> <button class="btn waves  modal-action modal-close waves-effect waves-green btn-flat"  id="prodDecBtn" type="button"> Send </button> </div>
      </div>

    </form>
  </div>
    </div>
    <div class="modal-footer">
      <a href="" class="">Agree</a>
    </div>
  </div>



</div>
<div class="row">
	<div class="col s10" id="allUsers"> </div>
</div>
<br><br><br><br>
<button class="waves btn" id="showOrders"> Show All Orders</button>
<div class="row">
  <div class="col s10" id="allOrders"> </div>
</div>
	

<div id="modal2" class="modal bottom-sheet">
   <div id="order_table"> </div>
    <div class="modal-footer">
      <a href="" class="">Agree</a>
    </div>
  </div>


  
<input type="hidden" id="base" value="<?= base_url()?>">
<input type="hidden" id="admin" value="<?= $admin ?>">






</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
<script src="<?=base_url('assets/admin.js');?>"> 

</script>
</html>