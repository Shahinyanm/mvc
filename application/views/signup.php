<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	 <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


  
</head>
<body>
	 <div class="row">
	
			<div class="col s6">
			<div class="row">
					<h3> Registration </h3>
			   		<form class="col s12" method="post" action="<?php echo base_url('users/register');?>">
			   			
			   		  <div class="row">
			   		    <div class="input-field col s6">
			   		      <input placeholder="firstname" id="first_name" type="text" class="validate" name="firstname" value="<?php echo set_value('firstname'); ?>">
			   		      <label for="first_name">First Name</label>
			   		    	<?php echo form_error('firstname'); ?>

			   		    </div>
			   		    <div class="input-field col s6">
			   		    	
			   		      <input id="last_name" type="text" class="validate" name="lastname" value="<?php echo set_value('lastname'); ?>">
			   		      <label for="last_name">Last Name</label>
			   		      <?php echo form_error('lastname'); ?>
			   		    </div>
			   		  </div>
			   		  
			   		  <div class="row">
			   		    <div class="input-field col s12">
			   		      <input id="password" type="password" class="validate" name="password" value="<?php echo set_value('password'); ?>" >
			   		      <label for="password">Password</label>
			   		      <?php echo form_error('password'); ?>
			   		    </div>
			   		  </div>
			   		  <div class="row">
			   		    <div class="input-field col s12">
			   		      <input id="passconf" type="password" class="validate" name="passconf" value="<?php echo set_value('passconf'); ?>">
			   		      <label for="password">confirm Password</label>
			   		      <?php echo form_error('passconf'); ?>
			   		    </div>
			   		  </div>
			   		  <div class="row">
			   		    <div class="input-field col s12">
			   		      <input id="email" type="email" class="validate" name="email" value="<?php echo set_value('email'); ?>">
			   		      <label for="email">Email</label>
			   		      <?php echo form_error('email'); ?>
			   		    </div>
			   		  </div>
			   		 
			   		  <div class="row">
			   		    <div class="input-field col s1">
			   		      <input type="number" class="validate" id="age" name="age" value="<?php echo set_value('age'); ?>">
			   		      <label for="age">Age</label>

			   		    </div>
   			   		    <div class="input-field col s12">
							<?php echo form_error('age'); ?>

			   			</div>
			   		  </div>
			   		  	<button class="btn waves-effect waves-light" type="submit" type="button"> Send
			   				<i class="material-icons right">send</i>
			 				</button>
			   		</form>
			 	</div>
			</div>

			<div class="col s4">
				<div class="row">
					<h3> Login </h3>
					<form class="col s12" method="post" action="<?php echo base_url('users/login');?>">
			   			
					<div class="row">
			   		   <div class="input-field col s12">
			   		      <input id="logEmail" type="email" class="validate" name="logEmail" value="<?php echo set_value('email'); ?>">
			   		      <label for="email">Email</label>
			   		      <?php echo form_error('email'); ?>
			   		   </div>
			   		</div>
			   		 

			   		  <div class="row">
			   		    <div class="input-field col s12">
			   		      <input id="logPassword" type="password" class="validate" name="logPassword" value="<?php echo set_value('password'); ?>" >
			   		      <label for="logPassword">Password</label>
			   		      <?php echo form_error('password'); ?>
			   		    </div>
			   		  </div>
			   		 
			   		  
			   		  
			   		  </div>

			   		  	<button class="btn waves-effect waves-light" type="submit" type="button"> Send
			   				<i class="material-icons right">send</i>
			 				</button>
			 			 <div class="row">
			   		    	<div class="input-field col s12">	
							<button id="forgetPassword" class="btn " type="button"> Remember password</button>
						</div>
					</div>
			 				 <div class="row" id="fgPassword" style="display: none;">
			   		    <div class="input-field col s12" >
			   		      <input id="fgemail" type="text" class="validate" name="fgemail" value="<?php echo set_value('fgemail'); ?>" >
			   		      <label for="fgp"></label>
			   		      <?php echo form_error('fgemail'); ?>
			   		      <button id="sendEmail" class="btn " type="button"> Send email</button>
			   		    </div>
			   		  </div>
			   		 
			   		  
			   		  
			   		  </div>
			   		</form>
			   		<?php if (isset($msg)){ echo "<div class=row'><div class='alert alert-success'>".$msg."</div></div>";}?>
			</div>
		</div>


		</div>


		 
			   		    
			   		      <?php if (isset($message)){ echo "<div class=row'><div class='alert alert-success'>".$message."</div></div>";}?>
			   		    
<input type="hidden" id="base" value="<?= base_url()?>">


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <script src="<?=base_url('assets/signup.js');?>">
  	
  	
  </script>
  
</html>