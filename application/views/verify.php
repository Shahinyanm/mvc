<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	 <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


  
</head>
<body>
	 <div class="row">
	
			<div class="col s6">
			<div class="row">
				<div class="col s4">
				<div class="row">
					<h3> Add new password </h3>
					<form class="col s12" method="post" action="<?php echo base_url('users/updatePassword');?>">
			   			
					<div class="row">
			   		   <div class="input-field col s12">
			   		      <input id="newPassword" type="password" class="validate" name="newPassword" value="<?php echo set_value('password'); ?>" >
			   		      <label for="newPassword">New Password</label>
			   		      <?php echo form_error('newPassword'); ?>
			   		   </div>
			   		</div>
			   		 

			   		  <div class="row">
			   		    <div class="input-field col s12">
			   		      <input id="confPassword" type="password" class="validate" name="confPassword" value="<?php echo set_value('password'); ?>" >
			   		      <label for="confPassword">Confirm Password</label>
			   		      <?php echo form_error('confPassword'); ?>
			   		    </div>
			   		  </div>
			   		 
			   		  
			   		  
			   		  </div>

			   		  	<button class="btn waves-effect waves-light" id="updPassword" type="submit"> Send
			   				<i class="material-icons right">send</i>
			 				</button>
			 			
			 			
			   		 
			   		  
			   		  
			   		  </div>
			   		</form>
			   		
			   		    
<input type="hidden" id="base" value="<?= base_url()?>">


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <script src="<?=base_url('assets/signup.js');?>">
  	
  	
  </script>
  
</html>