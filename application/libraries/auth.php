<?php 

	class auth {

		private $ci;


		function __construct(){

			$this->ci = &get_instance();
			$this->ci->load->library('session');
		}

		function is_logged(){
			if($this->ci->session->has_userdata('user')){
				
				return true;

			}else{

				return false;

			}
		}


		function is_admin(){

			if( $this->ci->session->has_userdata('admin')){
				$data =  $this->ci->session->userdata('admin');

				return $data == 1;
				
			}else{

				return false;
			}
		}

	}



 ?>