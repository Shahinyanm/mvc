<?php 
	
	class Products extends CI_Controller{

			function __construct(){

				parent::__construct();
				$this->load->model('products_model');
				$this->load->model('orders_model');
				$this->load->model('orders_products_model');
			}

			function new_product(){

				if(isset($_FILES['photo']['name'])){
					$config=[
						"upload_path"		=> "./uploads/",
						"allowed_types"		=>	"jpg|jpeg|png|gif"
					];
					$this->load->library('upload',$config);
					$this->upload->do_upload('photo');
					$user = $this->session->userdata('user'); 

					$tmp = [
						'name'			=>	$this->input->post('name'),
						'price'			=>	$this->input->post('price'),
						'description'	=>	$this->input->post('textarea'),
						'photo'			=>	$_FILES['photo']['name'],
						'user_id'		=>	$user->id
					];
					$this->products_model->add_products($tmp);
				}
				
			}

			function all(){

				$result = $this->products_model->find(['status'=>1]);
				echo json_encode($result);

			}
			
			function pay(){

				$ids 	= 	json_decode($this->input->post('prodId'));
				$result =	$this->products_model->find_price_from_products($ids);
				$user  	=	$this->session->userdata('user');

				 require_once('./config.php');

				 $token  = $this->input->post('stripeToken');
				 $email  =$this->input->post('stripeEmail');

				 $customer = \Stripe\Customer::create(array(
				     'email' => $email,
				     'source'  => $token
				 ));

				 $charge = \Stripe\Charge::create(array(
				     'customer' => $customer->id,
				     'amount'   => intval($result->price),
				     'currency' => 'usd'
				 ));


				 echo '<h1>Successfully charged'. $result->price.'</h1>';

				$res =  $this->orders_model->add_order(['user_id'=>$user->id,'time'=>date("Y-m-d H:i:s")]);
				foreach ($ids as $value) {
					$this->orders_products_model->add_order_product(['orders_id'=>$res,'products_id'=> $value]);
				}
				 

			}


			function show($page = 0){

				$result = $this->products_model->find(['status'=>1],4,($page-1)*4);

				// echo "<pre>";
				// print_r($result);
				// die;
				$tiv = ($this->products_model->get_amount_of_products()/4);
				
				$this->load->view('products',['result'=> $result,"cmount" => $tiv]);

			}


	}








 ?>