<?php 

class users extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('users_model');
		$this->load->model('messages_model');
		$this->load->model('invite_model');
		$this->load->helper('cookie');

	}

	function signup(){
		$this->load->view('signup');
	}

	function register(){

		$firstname			= $this->input->post('firstname');
		$lastname 			= $this->input->post('lastname');
		$age 				= $this->input->post('age');
		$password 			= $this->input->post('password');
		$passconf 			= $this->input->post('confirm_password');
		$email 				= $this->input->post('email');

		$this->form_validation->set_rules('firstname','Firstname','required|min_length[3]|max_length[12]', array(
			'required'			=> 	"%s line is empty",
			'min_length[3]'		=>	"%s error ",
			'max_length[12]'	=>	"%s is larger than 12 "

		));
		$this->form_validation->set_rules('lastname','Lastname','required|max_length[12]', array(
			'required'			=>	"%s line is empty",
			'max_length[12]'	=>	"%s is larger than 12 "

		));;
		$this->form_validation->set_rules('password', 'Password','required|matches[passconf]', array(
			'required'			=> 	"%s line is empty",
			'atches[password]'	=>	"%s do'nt confirm",

		));;
		$this->form_validation->set_rules('passconf', 'passconf','required|matches[password]', array(
			'required'			=> 	"%s line is empty",
			'matches[password]'	=>	"%s do'nt confirm",

		));;
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]', array(
			'required'			=> 	"%s line is empty",
			'valid_email'		=>	"%s is not valid",
			'is_unique'         =>  "%s is not unique"

		));;
		$this->form_validation->set_rules('age','age','required|numeric', array(
			'required'			=>	"%s line is empty",
			'numeric'			=>	"Write only number"

		));;


		if ($this->form_validation->run()==false){

			$this->load->view('signup');

		}else{

			$tmp = [
				'firstname'	=> 	$firstname,
				'lastname'	=> 	$lastname,
				'age'		=> 	$age,
				'password'	=>	password_hash($password,PASSWORD_DEFAULT),
				'email'		=>	$email
			];


			$this->users_model->add_user($tmp);


			$this->load->view('signup',['message'=> "We have sent you confirmation email"]);


			$result = $this->users_model->find(['email'=>$email]);
			if(!empty($result)){
				$token = md5($result->id.$result->firstname.$result->email);
				$link = base_url()."users/activate/".$result->id."/".$token;
				$msg  = "<a href='$link' > Click Here </a> To activate your profile";
				$this->sms->send($result->email,'activate your profile',$msg);
			}
		}
	}
	function login(){

		$email 				= $this->input->post('logEmail');
		$password 			= $this->input->post('logPassword');

			// echo $email,$password;	

			// if($email == "admin@admin.com" & $password == "admin"){

			// 	$this->session->set_userdata('admin', 1);
			// 	redirect('admin/dashboard');

			// }else {


		$result = $this->users_model->find(['email'=>$email]);
		if($result != null ){
			if($result->status==1){
				if($result->type == "admin" ){
					if(password_verify($password, $result->password)){

						redirect ('admin/dashboard');

					}else{
						$this->input->set_cookie('atempt', $this->input->cookie('atempt')+1,10);

						if($this->input->cookie('atempt')>3 ){	
							echo "duq gerazanceciq porceri qanaky";
						}	

						$this->load->view('signup',['msg'=> "Wrong  password"]);	
					}
				}else {

					
					if(time()>$result->blockTime){

						if(password_verify($password, $result->password)){

							$this->session->set_userdata('user', $result);
							redirect('users/profile');

						}else {

							$this->input->set_cookie('atempt', $this->input->cookie('atempt')+1,10);

								if($this->input->cookie('atempt')>3 ){

									echo "duq gerazanceciq porceri qanaky";
								}	

							$this->load->view('signup',['msg'=> "Wrong  password"]);
						}
					}else{
						
					

						$this->load->view('signup',['msg'=> "you are blocked until ".  date('Y/m/d H:i:s', $result->blockTime)]);
					}	
				}
				}else {

					$this->load->view('signup',['msg'=> "Wrong email or nickname"]);
					
				}	
			}else{
				$this->load->view('signup',['msg'=> "Check your email"]);
			}			
			// }
			
		}


		function profile(){

			if($this->auth->is_logged()){

				
				$this->load->view('profile');

			}else{
				
				show_404();
				// $this->load->view('signup');


			}
		}

		function restore(){

			$email = $this->input->post('email');

			$result = $this->users_model->find(['email'=>$email]);
			if(!empty($result)){
				$token = md5($result->id.$result->firstname.$result->email);
				$link = base_url()."users/verify/".$result->id."/".$token;
				$msg  = "<a href='$link' > Click Here </a> To change you password";
				$this->sms->send($result->email,'Change password',$msg);
			}

		}

		function activate($id,$token){
			
			$result = $this->users_model->find(['id'=>$id]);
			if(!empty($result)){

				if(md5($result->id.$result->firstname.$result->email) == $token){
					
					$data  = ['status'=>1];
					$where = ['id'=>$id];	
					$this->users_model->update($data,$where);
				// $this->load->view('signup');
					$this->load->view('activate.php',['msg'=>'Stacvec']);
				}
			}else{

				show_404();
			}


		}

		function verify($id,$token){

			$result = $this->users_model->find(['id'=>$id]);
			if(!empty($result)){

				if(md5($result->id.$result->firstname.$result->email) == $token){
					
					$this->session->set_userdata('current_user', $id);
					$this->load->view('verify.php');
				}
			}else{

				show_404();
			}

		}

		function updatePassword(){

			$id = $this->session->userdata('current_user');
				// var_dump($id);


			$this->form_validation->set_rules('newPassword', 'newPassword','required|matches[confPassword]', array(
				'required'				=> 	"%s line is empty",
				'atches[confPassword]'	=>	"%s do'nt confirm",

			));;
			$this->form_validation->set_rules('confPassword', 'confPassword','required', array(
				'required'			=> 	"%s line is empty",
				// 'matches[newPassword]'	=>	"%s do'nt confirm",

			));;

			if ($this->form_validation->run()==false){

				show_404();


			}else{

				$data  = ['password'=>password_hash($this->input->post('newPassword'),PASSWORD_DEFAULT)];
				$where = ['id'=>$id];	
				$this->users_model->update($data,$where);
				$this->load->view('signup');

			}

		}


		function notification(){

			$user = $this->session->userdata('user');
			$result = $this->messages_model->find(['user_id'=>$user->id]);
			echo json_encode($result);
		}


		function invite_friend(){
			$user 		= 	$this->session->userdata('user');
			$email  	= 	$this->input->post('email');
			$id 		=	$this->invite_model->insert(['email'=>$email,'user_id'=>$user->id]);

			$result = $this->users_model->find(['email'=>$email]);
			if(empty($result)){
				$token = md5($email.$id);
				$link = base_url()."users/order/".$id."/".$token;
				$msg  = "<a href='$link' > Click Here </a> Invite to our site";
				$this->sms->send($email,'Invite',$msg);
			}
		}

		function order($id,$token){

			$result 	= 	$this->invite_model->find(['id'=>$id]);
			if(!empty($result)){

				if(md5($result->email.$result->id) == $token){
					
					$this->session->set_userdata('current_user', $id);
					$this->load->view('verify.php');
				}
			}else{

				show_404();
			}
		}

		function upgrade(){

			$money 	= 	$this->input->post('money');
				// $result =	$this->products_model->find_price_from_products($ids);
			$user  	=	$this->session->userdata('user');
				// echo "<pre>";
				// var_dump($_POST);
				// die;
			if(!empty($user) & $money =="1000" ){
				$this->users_model->update(['type'=>'admin'],['id'=>$user->id]);
				
				require_once('./config.php');

				$token  = $this->input->post('stripeToken');
				$email  =$this->input->post('stripeEmail');

				$customer = \Stripe\Customer::create(array(
					'email' => $email,
					'source'  => $token
				));

				$charge = \Stripe\Charge::create(array(
					'customer' => $customer->id,
					'amount'   => $money,
					'currency' => 'usd'
				));
				$this->session->set_userdata('msg','TOchnia sax');
				redirect('users/signup');
			}

				 // echo '<h1>Successfully charged'. $result->price.'</h1>';

				// $res =  $this->orders_model->add_order(['user_id'=>$user->id,'time'=>date("Y-m-d H:i:s")]);
				// foreach ($ids as $value) {
				// 	$this->orders_products_model->add_order_product(['orders_id'=>$res,'products_id'=> $value]);
				// }


		}
		


	}

	?>