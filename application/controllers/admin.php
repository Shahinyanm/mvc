<?php 
class admin extends CI_Controller{

			function __construct(){
				parent::__construct();
				$this->load->model(['products_model','users_model','messages_model','orders_model','orders_products_model']);
				// $this->load->model('users_model');


			}

		
	function dashboard(){
		
		if($this->auth->is_admin()){
			$this->load->view('dashboard');
		}else{
			show_404();
			// $this->load->view('signup');
		}
	}

	function all_products(){
		$result = $this->products_model->find(['status'=>0]);
		echo json_encode($result);
		// $this->load->view('dashboard',['result'=> $result]);
		// return $result;
	}

	function change_status(){

		$this->products_model->change_status(['id'=>$this->input->post('id')]);
			
	}

	function all_users(){
		$result = $this->users_model->findAll();
		echo json_encode($result);
	}

	function block_users(){

		if(is_numeric($this->input->post('time'))){

			$blockTime = time()+( $this->input->post('time')*60);
			// echo $blockTime;
			$this->users_model->block_users(['blockTime'=>$blockTime], ['id'=>$this->input->post('id')]);
		 
		}
	}

	function delete_product(){
		$text 		= $this->input->post('text');
		$id 		= $this->input->post('id');
		$user_id	= $this->input->post('user_id');

			
			$this->products_model->delete_product(['id'=>$id]);
			$this->messages_model->add_message(['text'=>$text, 'user_id' => $user_id]);

		
			
	}

	function all_orders (){

		$result = $this->orders_model->find_orders();
		echo json_encode($result);

	}

	function show_orders_products(){

		$result = $this->orders_products_model->find_orders_products($this->input->post('id'));
		echo json_encode($result);

	}


}
 ?>