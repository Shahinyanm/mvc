<?php 
	
	class Users_Model extends CI_Model {

		private $table;

		function __construct(){

			parent::__construct();
			$this->table = "users";
			


		}

		public function add_user(Array $arr){
			// $this->load->library('database');
			$this->db->insert($this->table,$arr);
		}

		public function find(Array $arr){

			return $this->db->get_where($this->table,$arr)->row();


		}

		public function findAll(){

			return $this->db->get($this->table)->result();


		}

		public function block_users($data,$where){
		
		$this->db->update($this->table, $data,$where); // gives UPDATE
		}

		public function update($data,$where){

				$this->db->update($this->table,$data,$where ); // gives UPDATE

		}
	}






 ?>