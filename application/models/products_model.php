<?php 
	
	class Products_Model extends CI_Model {

		private $table;

		function __construct(){

			parent::__construct();
			$this->table = "products";
			


		}

		function find_price_from_products(Array $data){

			$stm = "";

			foreach($data as $d)
			{
				$stm .= "$d,";
			}
			$stm = substr($stm,0,-1);

			return $this->db->query("Select sum(price) as price from $this->table where id in ( $stm ) ")->row();
		}

		public function add_products(Array $arr){
			// $this->load->library('database');
			$this->db->insert($this->table,$arr);
		}

		public function find(Array $arr,$limit,$offset){

			return  $this->db->get_where($this->table, $arr, $limit, $offset)->result();


		}

		public function change_status(Array $arr){

			$this->db->set('status', 'status+1', FALSE);
		$this->db->where('id', $arr['id']);
		$this->db->update($this->table); // gives UPDATE mytable SET field = field+1 WHERE id = 2


		}

		public function delete_product(Array $arr){
			$this->db->where('id', $arr['id']);
			$this->db->delete($this->table);

		}

		public function get_amount_of_products(){
			return $this->db->query('Select count(*) from  $this->table');
		}
		{
			
		}

	}






 ?>